package ro.ubb.remoting.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import ro.ubb.remoting.common.Movie;
import ro.ubb.remoting.common.Service;

import java.util.List;

public class MovieClientServiceImpl implements Service<Movie> {
    @Autowired
    private Service service;

    @Override
    public List<Movie> getAll() {
        return service.getAll();
    }

    @Override
    public Movie getById(Long id) {
        Movie movie = (Movie) service.getById(id);
        return movie;
    }

    @Override
    public int save(Movie entity) {
        return service.save(entity);
    }

    @Override
    public int update(Movie entity) {
        return service.update(entity);
    }

    @Override
    public int delete(Movie entity) {
        return service.delete(entity);
    }
}
