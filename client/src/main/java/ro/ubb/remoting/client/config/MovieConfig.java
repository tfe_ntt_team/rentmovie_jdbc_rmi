package ro.ubb.remoting.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import ro.ubb.remoting.client.service.MovieClientServiceImpl;
import ro.ubb.remoting.common.Service;

@Configuration
public class MovieConfig {

    @Bean
    RmiProxyFactoryBean rmiProxyFactoryBean() {
        RmiProxyFactoryBean proxyFactoryBean = new RmiProxyFactoryBean();
        proxyFactoryBean.setServiceInterface(Service.class);
        proxyFactoryBean.setServiceUrl("rmi://localhost:1099/MovieService");
        return proxyFactoryBean;
    }

    @Bean
    MovieClientServiceImpl studentService(){
        return new MovieClientServiceImpl();
    }
}
