package ro.ubb.remoting.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import ro.ubb.remoting.common.Service;
import ro.ubb.remoting.server.repository.MovieRepository;
import ro.ubb.remoting.server.service.ServerServiceImpl;

@Configuration
public class MovieConfig {
    @Bean
    RmiServiceExporter rmiServiceExporter() {
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceName("MovieService");
        exporter.setServiceInterface(Service.class);
        exporter.setService(serverService());
        return exporter;
    }

    @Bean
    MovieRepository studentRepository() {
        return new MovieRepository();
    }

    @Bean
    Service serverService() {
        return new ServerServiceImpl();
    }
}
