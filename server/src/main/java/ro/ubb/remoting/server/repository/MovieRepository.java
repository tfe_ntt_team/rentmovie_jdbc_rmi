package ro.ubb.remoting.server.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import ro.ubb.remoting.common.Movie;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class MovieRepository implements Repository<Movie>{

    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public List<Movie> findAll() {
        String sql = "select * from movies";
        List<Movie> movies = jdbcOperations.query(sql, (rs, i) -> {
            Long id = rs.getLong("id");
            String title = rs.getString("title");
            String director = rs.getString("director");
            int year = rs.getInt("year");
            int price = rs.getInt("price");

            Movie movie = new Movie(id ,title, director, year, price);
            movie.setId(id);
            return movie;
        });

        return movies;
    }

    @Override
    public Movie findById(Long id) {
        return jdbcOperations.queryForObject(
                "select * from movies where id=?",
                new RowMapper<Movie>() {
                    @Nullable
                    @Override
                    public Movie mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Movie movie = new Movie();
                        movie.setId(rs.getLong("id"));
                        movie.setDirector(rs.getString("title"));
                        movie.setTitle(rs.getString("director"));
                        movie.setYear(rs.getInt("year"));
                        movie.setPrice(rs.getInt("price"));
                        return movie;
                    }
                }, id);
    }

    @Override
    public int save(Movie movie) {
        String sql = "insert into movies (title, director, year, price) values(?,?,?,?)";
        return jdbcOperations.update(sql, movie.getTitle(), movie.getDirector(), movie.getYear(), movie.getPrice());
    }

    @Override
    public int update(Movie movie) {
        String sql = "update movies set title=?, director=?, year=?, price=? where id=?";
        return jdbcOperations.update(sql ,movie.getTitle(), movie.getDirector(), movie.getYear(), movie.getPrice(), movie.getId());
    }

    @Override
    public int deleteById(Long id) {
        String sql = "delete from movies where id=?";
        return jdbcOperations.update(sql, id);
    }
}
