package ro.ubb.remoting.common;


public class Movie extends BaseEntity<Long>{
	private Long id;
	private String title;
    private String director;
    private int year;
    private int price;

    public Movie() {
    }


	public Movie(String title, String director, int year, int price) {
        this.director = title;
        this.title = director;
        this.year = year;
        this.price = price;
    }

	public Movie(Long id, String title, String director, int year, int price) {
		this.id = id;
		this.title = title;
		this.director = director;
		this.year = year;
		this.price = price;
	}


    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Movie{" +
				"id=" + id +
				" title='" + title + '\'' +
				", director='" + director + '\'' +
				", year=" + year +
				", price=" + price +
				'}';
	}

	public String convertToString() {
		return id + "," + title + "," + director + "," + year + "," + price;
	}

}

