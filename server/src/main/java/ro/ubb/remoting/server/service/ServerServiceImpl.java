package ro.ubb.remoting.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import ro.ubb.remoting.common.BaseEntity;
import ro.ubb.remoting.common.Service;
import ro.ubb.remoting.server.repository.Repository;

import java.util.List;

public class ServerServiceImpl<T extends BaseEntity<Long>> implements Service<T> {

    @Autowired
    private Repository<T> repository;

    @Override
    public List<T> getAll() {
        return repository.findAll();
    }

    @Override
    public T getById(Long id) {
        return repository.findById(id);
    }

    @Override
    public int save(T entity) {
        return repository.save(entity);
    }

    @Override
    public int update(T entity) {
        return repository.update(entity);
    }

    @Override
    public int delete(T entity) {
        return repository.deleteById(entity.getId());
    }
}
