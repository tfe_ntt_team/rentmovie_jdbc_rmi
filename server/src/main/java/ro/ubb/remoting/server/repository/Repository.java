package ro.ubb.remoting.server.repository;

import java.util.List;


public interface Repository<T> {
    List<T> findAll();
    T findById(Long id);
    int save(T entity);
    int update(T entity);
    int deleteById(Long id);
}
