package ro.ubb.remoting.common;

import java.util.List;

public interface Service<T extends BaseEntity<Long>> {

    List<T> getAll();

    T getById(Long id);

    int save(T entity);

    int update(T entity);

    int delete(T entity);
}
