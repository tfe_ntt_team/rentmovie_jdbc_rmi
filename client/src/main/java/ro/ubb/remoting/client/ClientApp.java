package ro.ubb.remoting.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.remoting.client.UI.ClientConsole;
import ro.ubb.remoting.client.config.MovieConfig;
import ro.ubb.remoting.client.service.MovieClientServiceImpl;

import java.sql.SQLException;

public class ClientApp {
    public static void main(String[] args) throws SQLException {

        AnnotationConfigApplicationContext context=
                new AnnotationConfigApplicationContext("ro.ubb.remoting.client.config");
        AnnotationConfigApplicationContext studentContext=
                new AnnotationConfigApplicationContext(MovieConfig.class);

        MovieClientServiceImpl movieClientServiceImpl = studentContext.getBean(MovieClientServiceImpl.class);
        movieClientServiceImpl.getAll()
                .forEach(System.out::println);
//        System.out.println(movieClientServiceImpl.getById(8L));


        ClientConsole clientConsole = new ClientConsole(movieClientServiceImpl);
        clientConsole.menu();

        System.out.println("bye client");
    }
}
