package ro.ubb.remoting.server;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.remoting.common.Service;
import ro.ubb.remoting.server.config.MovieConfig;
import ro.ubb.remoting.server.config.JdbcConfig;

public class ServerApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext studentContext=
                new AnnotationConfigApplicationContext(MovieConfig.class, JdbcConfig.class);

        Service service = studentContext.getBean(Service.class);
        service.getAll().forEach(System.out::println);

        System.out.println("bye server");
    }
}
