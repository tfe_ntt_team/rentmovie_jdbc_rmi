package ro.ubb.remoting.client.UI;

import ro.ubb.remoting.client.service.MovieClientServiceImpl;
import ro.ubb.remoting.common.Movie;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class ClientConsole {
    private MovieClientServiceImpl movieClientService;

    public ClientConsole(MovieClientServiceImpl movieClientService) {
        this.movieClientService = movieClientService;
    }

    private Scanner scanner = new Scanner(System.in);

	public void menu() throws SQLException {

		while(true){
			printMenu();
			System.out.println("Choose an optino :");
			Scanner userInput = new Scanner(System.in);
			int opt = 0;
			if(userInput.hasNextInt()){
				opt = userInput.nextInt();
			}
			boolean exit = false;
			switch(opt){
				case 1:
					addMovie();
					break;
				case 2:
					break;
				case 3:
					printMovie();
					break;
				case 4:
					delMovie();
					break;
				case 5:
					updateMovie();
					break;
			}

		}

	}




	private void printMenu() {

		System.out.println("1. Add Movie.");
		System.out.println("2. Add Client.");
		System.out.println("3. List of available movies.");
		System.out.println("4. Delete movie!");
		System.out.println("5. Update movie!");
		System.out.println("14. Exit");
		System.out.println();
	}


	private Movie readMovie() {

		System.out.println("Movie Title : ");
		String title = scanner.next();

		System.out.println("Director name : ");
		String director = scanner.next();

		System.out.println("Year greater than 1800 : ");
		int year = scanner.nextInt();

		System.out.println("Price : ");
		int price = scanner.nextInt();

		Movie movie = new Movie(title,director,year,price);

		return movie;

	}

	private void addMovie() {
		Movie newMovie = readMovie();
		int result = movieClientService.save(newMovie);
		if (result == 1)
			System.out.println("Save succeded");
		else
			System.out.println("Save failed");
	}

    private void printMovie() {
        List<Movie> movies = movieClientService.getAll();
        movies.forEach(System.out::println);
    }


    private void updateMovie() {
		System.out.println("Enter the id for update: ");
		Long id = scanner.nextLong();
		Movie movie = movieClientService.getById(id);
		System.out.println("The Movie is : \n" + movie);
		System.out.println("The new Title is : ");
		String title = scanner.next();
		System.out.println("The new Director is : ");
		String director = scanner.next();
		System.out.println("The new Year is : ");
		int year = scanner.nextInt();
		System.out.println("The new Price is : ");
		int price = scanner.nextInt();
		Movie newMovie = new Movie(title, director, year, price);
		newMovie.setId(id);
        int response = movieClientService.update(newMovie);
            if (response == 1)
                System.out.println("Update succeded");
            else
                System.out.println("Update failed");
    }


    private void delMovie() {
        System.out.println("Id of the movie to delete:");
        Long id = scanner.nextLong();
        Movie s = movieClientService.getById(id);
        int res = movieClientService.delete(s);
            String message = res == 1 ? "Delete succeded" : "Delete failed";
            System.out.println(message);
    }

}
